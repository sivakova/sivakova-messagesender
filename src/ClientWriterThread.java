import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.net.Socket;

public class ClientWriterThread extends Thread {

    private Socket socket;
    private String login;

    public ClientWriterThread(String login, Socket socket) {
        this.socket = socket;
        this.login = login;
    }

    public void run() {
        try {
            BufferedWriter socketWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            String xmlMessage = "";

            BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));

            while (true) {
                String strConsole = consoleReader.readLine();
                try {
                    xmlMessage = XMLClientEditor.makeClientMessage(new Message(login, strConsole));
                } catch (ParserConfigurationException | TransformerException e) {
                    e.printStackTrace();
                }
                socketWriter.write(xmlMessage);
                socketWriter.newLine();
                socketWriter.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
