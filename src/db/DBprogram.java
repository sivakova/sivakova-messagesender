package db;

import java.sql.*;

public class DBprogram {
    public static void main(String[] args) {
        try {
            Connection conn = DriverManager.getConnection("jdbc:postgresql://194.87.187.238/", "sivakova", "sivakova");
//            PreparedStatement preparedStatement = conn.prepareStatement("CREATE TABLE message (id serial PRIMARY KEY, login text NOT NULL, message text NOT NULL)");
//            preparedStatement.execute();
//            PreparedStatement preparedStatement1 = conn.prepareStatement("INSERT INTO message (login, message) VALUES ('firstUser', 'first message'), ('second User', 'second message')");
//            preparedStatement1.execute();
//            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM message");
//            ResultSet result = preparedStatement.executeQuery();
//            result.first();

//            String userlogin = "thirdUser";
//            String message = "Hello!";
//            PreparedStatement insert = conn.prepareStatement("INSERT INTO message (login, message) VALUES" +
//                    "(?, ?)");
//            insert.setString(1, userlogin);
//            insert.setString(2, message);
//            insert.executeUpdate();

           // Очистка таблицы
//            Statement statement = conn.createStatement();
//            statement.executeUpdate("TRUNCATE TABLE message");

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM message");
            ResultSet result = preparedStatement.executeQuery();

            while (result.next()){
                System.out.println(result.getString("login") + ": " + result.getString("message"));
            }
            //"INSERT INTO message (login, message) VALUES ('"+userlogin+"','"+message+"'); - нельзя


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
