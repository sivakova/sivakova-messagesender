import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.List;

public class ServerThread extends Thread {
    private Socket socket;
    private List<Socket> sockets;
    private DataBase dataBase;

    public ServerThread(Socket s, List<Socket> sockets, DataBase dataBase) {
        this.socket = s;
        this.sockets = sockets;
        this.dataBase = dataBase;
    }

    public void run() {
        String xmlMessage = "";
        Message helloMessage = new Message();
        String authMessage;
        try {
            BufferedReader socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            Boolean checker = false;
//            while (!checker) {
                //Получаем от пользователя логин и пароль
            Message messageWithLogPass = XMLClientEditor.makeAuthMessage(socketReader.readLine());
            authMessage = messageWithLogPass.toString();
            System.out.println(authMessage);

            BufferedReader bufferedReaderFromFile = new BufferedReader(new FileReader("passwords.txt"));
            String string;

            while ((string = bufferedReaderFromFile.readLine()) != null){
                if (authMessage.equals(string)){
                    checker = true;
                    ServerHelpers.writeToSocket(XMLClientEditor.makeAuthAnswerMessage("accepted"), socket);
                    helloMessage = new Message(messageWithLogPass.author, "", Message.Type.CONNECT);

                    //отправка истории:
//                    BufferedReader bufferedReaderHistory = null;
//                    try {
//                        bufferedReaderHistory = new BufferedReader(new FileReader("history.txt"));
//                        String stringHistory;
//                    while ((stringHistory = bufferedReaderHistory.readLine()) != null){
//                        ServerHelpers.writeToSocket(stringHistory, socket);
//                    }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                    ;

                    List<Message> history = dataBase.buildHistory();
                    ServerHelpers.writeToSocket(XMLClientEditor.makeHistoryMessageStr(history), socket);

                    sockets.add(socket);
                    ServerHelpers.writeToAll(XMLClientEditor.makeClientMessage(helloMessage), sockets, socket);
                    break;
                }
            }
            bufferedReaderFromFile.close();

            if (!checker) {
                ServerHelpers.writeToSocket(XMLClientEditor.makeAuthAnswerMessage("rejected"), socket);
                socket.close();
                return;
            }
//            }
            //xmlMessage = XMLClientEditor.makeClientMessage(helloMessage);
            //System.out.println(helloMessage.toString());
            //System.out.println(helloMessage.toString());
            //ServerHelpers.writeToAll(xmlMessage, sockets, socket);

            while (true) {
                String strSocket = socketReader.readLine();
                Message message = XMLClientEditor.makeClientMessage(strSocket);
                System.out.println(message.toString());

                //ServerHelpers.writeToFile(strSocket);
                dataBase.insertMessage(message.author, message.text);
                ServerHelpers.writeToAll(strSocket, sockets, socket);
            }
        } catch (SocketException e) {
            sockets.remove(socket);
            helloMessage.type = Message.Type.DISCONNECT;
            System.out.println(helloMessage.toString());
            try {
                xmlMessage = XMLClientEditor.makeClientMessage(helloMessage);
                ServerHelpers.writeToAll(xmlMessage, sockets, null);
            } catch (ParserConfigurationException | TransformerException e1) {
                e1.printStackTrace();
            }
        } catch (IOException | SAXException | TransformerException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
}
