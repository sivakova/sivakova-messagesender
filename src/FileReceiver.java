import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.*;
import java.io.*;
import java.util.Base64;

public class FileReceiver {
    public static void main(String[] args) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbf.newDocumentBuilder();
            Document document = builder.parse("out.xml");
            CDATASection cdata = (CDATASection) document.getElementsByTagName("message").item(0).getFirstChild();

            byte[] fileData = Base64.getDecoder().decode(cdata.getData());

            FileOutputStream out = new FileOutputStream("sendedFile.txt");
            out.write(fileData);
            out.flush();
            out.close();
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
    }
}
