import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.net.Socket;
import java.util.List;

public class ServerWriterThread extends Thread {
    private List<Socket> sockets;
    private DataBase dataBase;

    public ServerWriterThread(List<Socket> s, DataBase dataBase) {
        this.sockets = s;
        this.dataBase = dataBase;
    }

    public void run() {
        try {
            BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                try {
                    String strConsole = consoleReader.readLine();
                    Message message = new Message("Server", strConsole);
                    String xmlMessage = XMLClientEditor.makeClientMessage(message);

                    dataBase.insertMessage(message.author, message.text);
                    //ServerHelpers.writeToFile(xmlMessage);
                    ServerHelpers.writeToAll(xmlMessage, sockets, null);
                } catch (ParserConfigurationException | TransformerException e) {
                    e.printStackTrace();
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
