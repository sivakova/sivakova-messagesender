import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.Base64;

public class FileSender {
    public static void main(String[] args) {
        System.out.println("input file name: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            File file = new File(br.readLine());
            FileInputStream fileInput = new FileInputStream(file);
            byte[] fileData = new byte[(int)file.length()];
            fileInput.read(fileData);

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbf.newDocumentBuilder();
            Document document = builder.newDocument();

            Element root = document.createElement("message");
            CDATASection cdata = document.createCDATASection(Base64.getEncoder().encodeToString(fileData));
            document.appendChild(root);
            root.appendChild(cdata);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            transformer.transform( new DOMSource(document), new StreamResult(System.out));//(new File("out.xml");

        } catch (IOException | ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }

    }
}
