import org.w3c.dom.*;
import javax.xml.stream.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

public class XmlDocumentSender {
    XMLStreamWriter xmlStreamWriter;
    Transformer transformer;
    OutputStream outputStream;

    public XmlDocumentSender (OutputStream outputStream) throws XMLStreamException, TransformerConfigurationException {
        this.outputStream = outputStream;
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformer = transformerFactory.newTransformer();

        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newFactory();
        xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(outputStream);  //в него пишем блоки cdata
        //Подключается клиент устонавливается сокет, повер него мы устонавливаем XMLDocumentSender и Reciever
        xmlStreamWriter.writeStartDocument();
        xmlStreamWriter.writeStartElement("message");
    }

    public void send (Document document) throws TransformerException, XMLStreamException {
        StringWriter stringWriter = new StringWriter();
        //Запихивать все составные xml документы в блок cdata
        transformer.transform(new DOMSource(document), new StreamResult(stringWriter));
        xmlStreamWriter.writeCData(stringWriter.toString());
        xmlStreamWriter.flush();
    }

    public void close () {
        try {
            xmlStreamWriter.writeEndDocument();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }
}
