public class Message {
    enum Type { TEXT, CONNECT, DISCONNECT, FILE, HISTORY}

    public String author;
    public String text;
    public Type type;
    public String password;
    public String status;

    public Message() {
        this.author = "";
        this.password = "";
        this.text = "";
        this.type = Type.TEXT;
    }

    public Message(String author, String text) {
        this.author = author;
        this.password = "";
        this.text = text;
        this.type = Type.TEXT;
    }

    public Message(String author, String text, Type type) {
        this.author = author;
        this.password = "";
        this.text = text;
        this.type = type;
    }

    public String toString() {
        switch (type) {
            case HISTORY:
                return text;
            case CONNECT:
                return author + ": Connected;";
            case DISCONNECT:
                return author + ": Disconnected";
            case FILE:
                return author + ": File transferred";
            case TEXT:
            default:
                return author + ": " + text;
        }
    }
}
