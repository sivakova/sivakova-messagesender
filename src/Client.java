import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.List;

import static javafx.application.Platform.exit;

public class Client {


    public static void main(String[] args) {

        try {
            Socket socket = new Socket();

            while (true){
                System.out.println("Enter your name:");
                BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
                String login = consoleReader.readLine();
                System.out.println("Enter your password:");
                String password = consoleReader.readLine();
                //System.out.println("Hello " + login + "!");


                socket = new Socket("127.0.0.1", 5553);

                //первая отправка на сервер
                BufferedWriter socketWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

                String xmlMessage = "";
                try {
                    xmlMessage = XMLClientEditor.makeAuthMessage(new Message(login, password));
                } catch (ParserConfigurationException | TransformerException e) {
                    e.printStackTrace();
                }
                socketWriter.write(xmlMessage);
                socketWriter.newLine();
                socketWriter.flush();
                //окончание первой отправки. Если не подключились нльзя идти дальше

                BufferedReader socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String authAnswer = socketReader.readLine();
                Message answer = XMLClientEditor.makeAuthMessage(authAnswer);
                System.out.println(answer.status);

                if (answer.status.equals("accepted")){
                    ClientWriterThread clientWriterThread = new ClientWriterThread(login, socket);
                    clientWriterThread.start();
                    break;
                } else {
                    socket.close();
                }
            }

            BufferedReader socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String strSocket = socketReader.readLine();
            List<Message> messages = XMLClientEditor.makeHistoryMessage(strSocket);

            for (Message message : messages) {
                System.out.println(message.toString());
            }

            while (true) {
                strSocket = socketReader.readLine();
                Message message = XMLClientEditor.makeClientMessage(strSocket);
                System.out.println(message.toString());
            }
        } catch (SocketException e){
            System.out.println("Server disconnect");
            exit();
        } catch (IOException | SAXException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
}