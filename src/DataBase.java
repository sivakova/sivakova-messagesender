import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataBase {
    public Connection conn;

    public DataBase() {
        try {
            this.conn = DriverManager.getConnection("jdbc:postgresql://194.87.187.238/", "sivakova", "sivakova");
        } catch (SQLException e) {
            System.out.println("Не удалось подключиться к базе данных.");
        }
    }

    public void clear(){
        try {
            Statement statement = conn.createStatement();
            statement.executeUpdate("TRUNCATE TABLE message");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertMessage(String author, String text){
        try {
            PreparedStatement insert = conn.prepareStatement("INSERT INTO message (login, message) VALUES" + "(?, ?)");
            insert.setString(1, author);
            insert.setString(2, text);
            insert.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Не удалось записать сообщение");
        }
    }

    public List<Message> buildHistory(){
        StringBuilder stringBuilder = new StringBuilder();
        List<Message> messages = new ArrayList<>();

        try {

            PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM message");
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()){
                messages.add(new Message(result.getString("login"), result.getString("message")));
//                stringBuilder.append(result.getString("login") + ": " + result.getString("message"));
//                stringBuilder.append("\n");
            }
        } catch (SQLException e) {
            System.out.println("Не удалось напечатать историю.");
        }
        return messages;
    }

    public void close(){
        try {
            conn.close();
        } catch (SQLException e) {
            System.out.println("не удалось закрыть базу данных");
        }
    }
}
