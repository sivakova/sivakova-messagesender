import java.io.*;
import java.net.Socket;
import java.sql.*;
import java.util.List;

public class ServerHelpers {
    public static void writeToAll (String string, List<Socket> sockets, Socket socketExcept){
        for (Socket socket : sockets) {
            if (socketExcept != socket) {
                try {
                    BufferedWriter socketWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                    socketWriter.write(string);
                    socketWriter.newLine();
                    socketWriter.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void writeToFile (String string){
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter("history.txt", true));
            bw.write(string);
            bw.newLine();
            bw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void writeToSocket(String string, Socket socket){
                try {
                    BufferedWriter socketWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                    socketWriter.write(string);
                    socketWriter.newLine();
                    socketWriter.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
    }
}
