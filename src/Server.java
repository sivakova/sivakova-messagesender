import java.io.*;
import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

public class Server {
    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(5553);
            List<Socket> sockets = new ArrayList<>();
            DataBase dataBase = new DataBase();
            //dataBase.clear();

            ServerWriterThread serverWriterThread = new ServerWriterThread(sockets, dataBase);
            serverWriterThread.start();

            System.out.println("Server: Server started");

            Socket clientSocket;
            while (true) {
                clientSocket = serverSocket.accept();


                ServerThread serverThread = new ServerThread(clientSocket, sockets, dataBase);
                serverThread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


