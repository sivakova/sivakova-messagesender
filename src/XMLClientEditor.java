import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public class XMLClientEditor {

    static public String makeClientMessage(Message message) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = dbf.newDocumentBuilder();
        Document document = builder.newDocument();
        document.setXmlVersion("1.0");
        document.setXmlStandalone(false);

        Element root = document.createElement("message");
        Attr author = document.createAttribute("author");
        Attr type = document.createAttribute("type");
        author.setValue(message.author);
        type.setValue(message.type.name());
        root.setAttributeNode(author);
        root.setAttributeNode(type);
        Text messageText = document.createTextNode(message.text);
        root.appendChild(messageText);

        document.appendChild(root);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        StringWriter outWriter = new StringWriter();
        transformer.transform(new DOMSource(document), new StreamResult(outWriter));
        StringBuffer sb = outWriter.getBuffer();
        return sb.toString();
    }

    static public Message makeClientMessage(String xmlString) throws ParserConfigurationException, IOException, SAXException {
        Message message = new Message();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        StringBuilder xmlStringBuilder = new StringBuilder();
        xmlStringBuilder.append(xmlString);
        ByteArrayInputStream input =  new ByteArrayInputStream(xmlStringBuilder.toString().getBytes("UTF-8"));
        Document doc = builder.parse(input);
        Element messageElement = doc.getDocumentElement();

        message.author = messageElement.getAttribute("author");
        message.text = messageElement.getTextContent();
        message.type = Message.Type.valueOf(messageElement.getAttribute("type"));

        return message;
    }

    static public String makeAuthMessage(Message message) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = dbf.newDocumentBuilder();
        Document document = builder.newDocument();
        document.setXmlVersion("1.0");
        document.setXmlStandalone(false);

        Element root = document.createElement("authorisation");
        Attr author = document.createAttribute("login");
        Attr password = document.createAttribute("password");

        author.setValue(message.author);
        password.setValue(message.password);

        root.setAttributeNode(author);
        root.setAttributeNode(password);

        Text messageText = document.createTextNode(message.text);
        root.appendChild(messageText);

        document.appendChild(root);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        StringWriter outWriter = new StringWriter();

        DOMSource ds = new DOMSource(document);
        StreamResult sr = new StreamResult(outWriter);

        transformer.transform(ds, sr);
        StringBuffer sb = outWriter.getBuffer();
        return sb.toString();
    }

    static public Message makeAuthMessage(String xmlString) throws ParserConfigurationException, IOException, SAXException {
        Message message = new Message();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        StringBuilder xmlStringBuilder = new StringBuilder();
        xmlStringBuilder.append(xmlString);
        ByteArrayInputStream input =  new ByteArrayInputStream(xmlStringBuilder.toString().getBytes("UTF-8"));
        Document doc = builder.parse(input);
        Element messageElement = doc.getDocumentElement();

        message.author = messageElement.getAttribute("login");
        message.password = messageElement.getAttribute("password");
        message.status = messageElement.getAttribute("status");
        message.text = messageElement.getTextContent();
//        message.type = Message.Type.valueOf(messageElement.getAttribute("type"));

        return message;
    }

    static public String makeAuthAnswerMessage(String statusValue) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = dbf.newDocumentBuilder();
        Document document = builder.newDocument();
        document.setXmlVersion("1.0");
        document.setXmlStandalone(false);

        Element root = document.createElement("authorisation");
        Attr status = document.createAttribute("status");

        status.setValue(statusValue);

        root.setAttributeNode(status);

        Text messageText = document.createTextNode("");
        root.appendChild(messageText);

        document.appendChild(root);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        StringWriter outWriter = new StringWriter();

        DOMSource ds = new DOMSource(document);
        StreamResult sr = new StreamResult(outWriter);

        transformer.transform(ds, sr);
        StringBuffer sb = outWriter.getBuffer();
        return sb.toString();
    }


    static public String makeHistoryMessageStr(List<Message> history) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = dbf.newDocumentBuilder();
        Document document = builder.newDocument();
        document.setXmlVersion("1.0");
        document.setXmlStandalone(false);

        Element root = document.createElement("history");


        for (Message message : history) {

            Element messageElement = document.createElement("message");
            Attr author = document.createAttribute("author");
            Attr type = document.createAttribute("type");

            author.setValue(message.author);
            type.setValue(message.type.name());
            messageElement.setAttributeNode(author);
            messageElement.setAttributeNode(type);
            Text messageText = document.createTextNode(message.text);
            messageElement.appendChild(messageText);

            root.appendChild(messageElement);
        }
        document.appendChild(root);

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        StringWriter outWriter = new StringWriter();

        transformer.transform(new DOMSource(document), new StreamResult(outWriter));
        StringBuffer sb = outWriter.getBuffer();

        return sb.toString();
    }

    static public List<Message> makeHistoryMessage(String xmlHistory) throws ParserConfigurationException, IOException, SAXException {
        List<Message> messages = new ArrayList<>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();


        ByteArrayInputStream input =  new ByteArrayInputStream(xmlHistory.getBytes("UTF-8"));
        Document doc = builder.parse(input);
        Element historyElement = doc.getDocumentElement();
        NodeList messageNodes = historyElement.getChildNodes();


        for (int i = 0; i < messageNodes.getLength(); i++) {
            Element messageElement = (Element)messageNodes.item(i);
            Message message = new Message();

            message.author = messageElement.getAttribute("author");
            message.text = messageElement.getTextContent();
            message.type = Message.Type.valueOf(messageElement.getAttribute("type"));

            messages.add(message);
        }

        return messages;
    }
}
